﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyTest
{
   
    public class StudentInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("before-----");
            try
            {
               // Console.WriteLine(">>" + invocation.Method.Name);
                invocation.Proceed();
            }
            catch
            {
                //...                
            }
            Console.WriteLine("after---");
        }
    }

    public class SomeInterceptor2 : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("before2---Intercept");
            try
            {
                invocation.Proceed();
            }
            catch
            {
                //...                
            }
            Console.WriteLine("after2---Intercept");
        }
    }


}
