﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProxyTest
{
    public class InterceptorSelector : IInterceptorSelector
    {
        /// <summary>
        /// 这里可以决定什么的方法返回什么样的拦截器
        /// </summary>
        /// <param name="type"></param>
        /// <param name="method"></param>
        /// <param name="interceptors"></param>
        /// <returns></returns>
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            return interceptors;
            //if (method.Name.StartsWith("DoSome2"))
            //{
            //    return interceptors;    //这里可以决定什么的方法返回什么样的拦截器
            //}
            //else
            //{
            //    return interceptors.Where(i => i is StudentInterceptor).ToArray<IInterceptor>();

            //}
        }
    }

    public class InterceptorFilter : IProxyGenerationHook
    {
        /// <summary>
        /// 什么样的方法进行代理
        /// </summary>
        /// <param name="type"></param>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public bool ShouldInterceptMethod(Type type, MethodInfo memberInfo)
        {
            return true; 
            return memberInfo.Name.StartsWith("Eat");
         //   return memberInfo.IsSpecialName &&
           //     (memberInfo.Name.StartsWith("set_") || memberInfo.Name.StartsWith("get_"));   //什么样的方法可以被拦截?
        }
        public void NonVirtualMemberNotification(Type type, MemberInfo memberInfo)
        {
            //类型为非虚方法时,这里可以得到提示,将被调用
        }
        public void MethodsInspected()
        {
        }

        public void NonProxyableMemberNotification(Type type, MemberInfo memberInfo)
        {
            throw new NotImplementedException();
        }
    }

}
