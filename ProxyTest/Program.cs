﻿using Castle.DynamicProxy;
using System;

namespace ProxyTest
{
    class Program
    {
        static void Main(string[] args)
        {

            ProxyGenerator proxyGenerator = new ProxyGenerator();
            var options = new ProxyGenerationOptions(new InterceptorFilter()) { Selector = new InterceptorSelector() };

            //  ISome proxyClass = proxyGenerator.CreateClassProxy<SomeImp>(new SomeInterceptor(), new SomeInterceptor2());
            // object[] objs = { "dddd" };
            // SomeImp proxyClass = (SomeImp) proxyGenerator.CreateClassProxy(typeof(SomeImp), objs, new SomeInterceptor());

            Student proxyClass = proxyGenerator.CreateClassProxy<Student>(options,new StudentInterceptor(), new SomeInterceptor2());  //这里可以指定多个拦截器


            proxyClass.name = "大神吗";
            Console.WriteLine("------------------------------");
            //Console.WriteLine("The entity  is :" + proxyClass);
            //Console.WriteLine("Type of the entity: " + proxyClass.GetType().FullName);


            proxyClass.Eat();
            Console.WriteLine("---------------2---------------"); 
            proxyClass.Sleep();

            Console.WriteLine("------------------------------");
            proxyClass.PrintTest();


            //ISome some = new SomeImp();
            //some.DoSome();

            Console.ReadKey();
        }
    }



    public interface IPerson
    {

        void Sleep();

        void Eat();

        void PrintTest();

    }

    //被代理类，也就是需要拦截这个类中的方法
    //往这个类中添加功能
    public class Student : IPerson
    {
        private string _s;

        public Student()
        {

        }

        /// <summary>
        /// 一定要是虚方法（virtual）才会被拦截
        /// </summary>
        public virtual string name { get; set; }

        public Student(string s)
        {
            this._s = s;
        }

        /// <summary>
        /// 一定要是虚方法（virtual）才会被拦截
        /// </summary>
        public virtual void Sleep()
        {
            Console.WriteLine($"睡觉 ... ");
        }

        public virtual void Eat()
        {
            Console.WriteLine($"吃饭 ...{_s}");
        }

        public virtual void PrintTest()
        {
            Console.WriteLine("print-test");

        }

        
    }

}




